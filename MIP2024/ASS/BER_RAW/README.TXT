
    ////////////////////////////////////////////////////////////

    Last Cropped Image : Berserk_1_016.jpg ( 2024_JAN_12TH )
    Next Image To Crop : Berserk_1_017.jpg

    ////////////////////////////////////////////////////////////

    Let's not make my[ MKB ]repo take up an offensive 
    amount of megabytes like my[ AHK03 ]repo . That
    makes it a bit hard to manage .

    Let's try to keep each asset folder to about 50 MB max .
    With a hard limit of 100 MB . 

    100 MB in a folder : OKAY
    101 MB in a folder : ABSOLUTELY UNACCEPTABLE NO EXCEPTIONS

    If we need larger data sets , we will create a dedicated
    [ MKB_ASS ]repo for[ Magic_Kyoot_Ball ]Assets .

    @MKB@ : Magic Kyoot Ball , name of a game I want to make .
    @KBE@ : Kyoot_Bot_Engine , name of this C99 engine project .

